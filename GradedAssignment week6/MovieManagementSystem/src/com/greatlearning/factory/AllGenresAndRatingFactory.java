package com.greatlearning.factory;

import com.greatlearning.interfaces.AllGenresAndRating;
import com.greatlearning.moviescomingdao.GenresRatingMC;
import com.greatlearning.moviesintheatersdao.GenresRatingMT;
import com.greatlearning.topratingindiandao.GenresRatingTI;
import com.greatlearning.topratingmoviesdao.GenresRatingTM;

public class AllGenresAndRatingFactory {

	public static AllGenresAndRating createAllGenresAndRating(String channel) {
		if (channel == null || channel.isEmpty())
			return null;
		if ("moviescoming_genres_rating".equals(channel)) {
			return new GenresRatingMC();
		} else if ("moviesinTheater_genres_rating".equals(channel)) {
			return new GenresRatingMT();
		} else if ("TopRatedIndia_genres_rating".equals(channel)) {
			return new GenresRatingTI();
		} else if ("topRatedMovies_genres_rating".equals(channel)) {
			return new GenresRatingTM();
		}

		return null;

	}
}
