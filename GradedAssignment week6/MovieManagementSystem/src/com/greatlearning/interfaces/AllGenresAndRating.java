package com.greatlearning.interfaces;

import java.sql.SQLException;
import java.util.List;

import com.greatlearning.bean.GenresAndRating;
//import com.greatlearning.bean.Movies;

public interface AllGenresAndRating {
	public List<GenresAndRating> getMovies() throws SQLException;

	public int insert(GenresAndRating movie) throws SQLException;
	 
	public int update(GenresAndRating update) throws SQLException;
	
	public int delete(GenresAndRating delete) throws SQLException;
}
